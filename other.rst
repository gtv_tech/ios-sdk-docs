.. GTVSDKDoc documentation master file, created by
   sphinx-quickstart on Dec Wed 30 15:00:00 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
Other
=====================================

Share Link to Facebook
^^^^^^^^^^^^^^^^^^^^^^^

|
| *Objective-c syntax*
-------------

.. code-block:: objective-c

	//#import <GTVLib/GTVLib.h> // through cocoaPods
	#import <GTVFramework/GTVManager.h> // without cocoaPods


.. code-block:: objective-c

   [[GTVManager sharedManager] shareLinkContent:@"https://gtv.com.vn/" fromViewController:self callback:^(NSDictionary<NSString *,id> * _Nullable results, NSError * _Nullable error, BOOL cancel) {
      NSLog(@"\nResults: %@ \n Error: %@ \n Cancel: %d", results, error, cancel);
   }];


|
| *Swift syntax*
-------------

.. code-block:: Swift

   GTVManager.shared().shareLinkContent("https://gtv.com.vn/", from: self) { (result, erroe, cancel) in
      //
   }