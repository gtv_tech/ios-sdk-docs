.. GTVSDKDoc documentation master file, created by
   sphinx-quickstart on Thu Mar 14 15:46:07 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Login & logout
=====================================

Get UserInfo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. After login, register or relaunch the game (logged in before), you can get user basic infomation (uuid, user name) and tokens for payment or account

|
| *Objective-c syntax*
-------------

.. code-block:: objective-c

	NSString *uuid = [[GTVManager sharedManager] getUUID];
	NSString *userName = [[GTVManager sharedManager] getUserName];
	NSString* acc_token = [[GTVManager sharedManager] getAccountToken];
	NSString* pay_token = [[GTVManager sharedManager] getPaymentToken];


|
| *Swift syntax*
-------------

.. code-block:: Swift

	let uuid = GTVManager.shared().getUUID()
	let userName = GTVManager.shared().getUserName()
	let acc_token = GTVManager.shared().getAccountToken()
	let pay_token = GTVManager.shared().getPaymentToken()


2. using userHash to call API get user info in Server side docs


**Note: We will send server api document privately**

Login
^^^^^^

|
| *Objective-C syntax*
-------------

Import

.. code-block:: objective-c

	//#import <GTVLib/GTVLib.h> // through cocoaPods
	#import <GTVFramework/GTVManager.h> // without cocoaPods


Show login

After login success, we send back uuid and username for you. And you can get tokens (AccountTokent and PaymentToken) that use for us server.

.. code-block:: objective-c

	[[GTVManager sharedManager] showLogin:^(BOOL isSuccess, NSString * _Nullable message, NSString * _Nullable uuid, NSString * _Nullable username) {
		//
		if (isSuccess && uuid != nil) {
			NSLog(@"gtv login success: %@", uuid);
			dispatch_async(dispatch_get_main_queue(), ^{
				GameViewController *gameVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GameViewController"];
				[self.navigationController pushViewController:gameVC animated:YES];
			});
		} else {
			dispatch_async(dispatch_get_main_queue(), ^{
			// offending code goes in here
			UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"Login error : %@", message] preferredStyle:UIAlertControllerStyleAlert];
				[alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
				[self presentViewController:alert animated:YES completion:nil];
			});
		}
	}];


|
| *Swift syntax*
-------------

.. code-block:: Swift

	GTVManager.shared().showLogin { (isSuccess, message, uuid, username) in
	 	//
	}

Logout
^^^^^^

|
| *Objective-C syntax*
-------------

Import

.. code-block:: objective-c

	//#import <GTVLib/GTVLib.h> // through cocoaPods
	#import <GTVFramework/GTVManager.h> // without cocoaPods


Logout

.. code-block:: objective-c

	[[GTVManager sharedManager] logout:^(BOOL isSuccess, NSString * _Nullable message) {
		//
		if (isSuccess) {
			UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Logout success!" preferredStyle:UIAlertControllerStyleAlert];
			[alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
			[self presentViewController:alert animated:YES completion:nil];
		}

	}];

|
| *Swift syntax*
-------------

.. code-block:: Swift

	GTVManager.shared().logout { (isSuccess, message) in
		//
	}


**Note: If you use us tokens(UserToken and PaymentToken) but this token is exprired, you need to call refresh it**

|
| *Objective-C syntax*
-------------

.. code-block:: objective-c

	[[GTVManager sharedManager] refreshTokenPaymentNow:^(BOOL isSuccess, NSString * _Nullable message) {
		//
	}];

	[[GTVManager sharedManager] refreshTokenAccountNow:^(BOOL isSuccess, NSString * _Nullable message) {
		//
	}];

|
| *Swift syntax*
-------------

.. code-block:: Swift

	GTVManager.shared().refreshTokenPaymentNow { (isSuccess, message) in
		//
	}
	GTVManager.shared().refreshTokenAccountNow { (isSuccess, message) in
		//
	}