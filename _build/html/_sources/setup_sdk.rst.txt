.. GTVSDKDoc documentation master file, created by
   sphinx-quickstart on Thu Mar 14 15:46:07 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Setup SDK & Environment
=====================================

Technical Requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
iOS 10 as a deployment target.

Required Information
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Some information we must provided for the partner to use. Please ask for it if you don't see it. They are:

- Facebook App Id
- Firebase google-services.json file
- Firebase GoogleService-Info.plist file
- SDK workflow and server api documentation
- Android keystore and password for it
- Apple provision profile (Adhoc and Production)
- Apple certificate and password for it
- GTVconfig for info.plist file.


Install 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

GTVFramework is available through CocoaPods. To install it, simply add the following line to your Podfile:

.. code-block:: objective-c

	pod 'GTVFramework'


If you don't want to use this framework through cocoaPods. You can set up manually:

1. Drap and drop GTVFramework.framework to your project.

2. In tab "General", expand "Framework, Libraries, and Embedded content" you can see GTVFramework.framework, choose "Embed & sign".

3. Add frameworks require by GTVFramwork : AFFoundation.framework, CFNetwork.framework, libc++.tbd, libicucore.tbd, MapKit.framework, StoreKit.framework, SystemConfiguration.framework, WebKit.framework.

.. image:: /images/embedded_framework.png

**Note: immortal**

If you setup Framework without cocoaPods, you need change all import header (`#import <GTVLib/GTVLib.h>`) to:

.. code-block:: objective-c

	#import <GTVFramework/GTVManager.h>


Import 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

|
| *With Objective-c*
------------

In AppDelegate.m, didFinishLaunchingWithOptions method add these following code to setup the SDK

.. code-block:: objective-c

	//#import <GTVLib/GTVLib.h> // through cocoaPods
	#import <GTVFramework/GTVManager.h> // without cocoaPods

.. code-block:: objective-c

	- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
		// Override point for customization after application launch.
		[[GTVManager sharedManager] ImplementWith:application didFinishLaunchingWithOptions:launchOptions useProductServer:NO];// YES to switch to production, NO to switch to development 
		return YES;
	}

|
| *With Swift*
------------

With Swift project, you need create a Bridging-Header.h file first.
Then import GTVFramework in Bridging-Header.h

.. code-block:: objective-c

	//#import <GTVLib/GTVLib.h> // through cocoaPods
	#import <GTVFramework/GTVManager.h> // without cocoaPods

In AppDelegate.swift, implement GTVFramework

.. code-block:: swift

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		GTVManager.shared().implement(with: application, didFinishLaunchingWithOptions: launchOptions!, useProductServer: false)
		return true
	}



Info.plist config
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Now configure the .plist for your project: In Xcode right-click your .plist file and choose “Open As Source Code”.Copy & Paste the XML snippet into the body of your file (…).
– Config for login via facebook

.. code-block:: xml
	
	<key>FacebookAppID</key>
	<string>{FACEBOOK_APP_ID}</string>
	<key>FacebookDisplayName</key>
	<string>{FACEBOOK_APP_NAME}</string>
	<key>CFBundleURLTypes</key>
	<array>
	    <dict>
	        <key>CFBundleTypeRole</key>
	        <string>Editor</string>
	        <key>CFBundleURLName</key>
	        <string></string>
	        <key>CFBundleURLSchemes</key>
	        <array>
	            <string>fb{Facebook_App_Id}</string>
	        </array>
	    </dict>
	    <dict>
	        <key>CFBundleTypeRole</key>
	        <string>Editor</string>
	        <key>CFBundleURLName</key>
	        <string></string>
	        <key>CFBundleURLSchemes</key>
	        <array>
	            <string>{Bundle_Identifier}</string>
	        </array>
	    </dict>
	</array>

**Note: {Facebook_App_Id} and {Bundle_Identifier}, we will send them privately for you**


GoogleService-Info.plist and GTVService-Info.plist
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Config file GoogleService-Info.plist and GTVService-Info.plist

    . Copy files GoogleService-Info.plist and GTVService-Info.plist into the root of your Xcode project. If prompted, select to add the config file to all targets.

.. image:: /images/gg_gtv_infoplist.png

- Config App Transport Security
GTVFramework use google login. You need to set URL schema for google login.
Open file GoogleService-Info.plist and copy short text , It's like that :

.. code-block:: xml

	com.googleusercontent.apps.542996191572-8h0go6lss5pivtbqj44g371s51gnhonm

and add to URL schema.

.. image:: /images/google-schema.png

**Note: This files (GoogleService-Info.plist and GTVService-Info.plist)  we will send it privately for you**

- Config App Transport Security

.. code-block:: xml

	<key>NSAppTransportSecurity</key>
	<dict>
		<key>NSAllowsArbitraryLoads</key>
		<true/>
	</dict>


Enable Push Notification
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Goto Capabilities -> Push Notification -> Turn On

.. image:: /images/img4.png

With new Xcode version(> Version 11.4)

.. image:: /images/notification_xcode_new.png


Enable Background Mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Goto Capabilities -> Background Modes -> Turn On
Check to Background fetch, Remote notifications

.. image:: /images/img5.png

With new Xcode version(> Version 11.4)

.. image:: /images/bg_add_new_xcode.png
.. image:: /images/bg_selected_new_xcode.png



Enable Sign In with Apple
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Goto Capabilities -> Sign In with Apple -> Turn On

With new Xcode version(> Version 11.4)

.. image:: /images/apple_login.png



Setup handle notification and login facebook
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

|
| *With Objective-C*
-------------

In AppDelegate.m

.. code-block:: objective-c

	- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
		bool shouldOpen = [[GTVManager sharedManager] gtvApplication:app openURL:url options:options];
		return shouldOpen;
	}

	- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {

		// Handle notify.
		[[GTVManager sharedManager] handleNotificationsWithDeviceToken:deviceToken];
	}

|
| *If you use SceneDelegate in your project, let set this handle at call back openURLContexts.*

.. code-block:: objective-c

	- (void)scene:(UIScene *)scene openURLContexts:(NSSet<UIOpenURLContext *> *)URLContexts API_AVAILABLE(ios(13.0)){
		NSURL *url = [[URLContexts allObjects] firstObject].URL;
		bool shouldOpen = [[GTVManager sharedManager] gtvApplication:[UIApplication sharedApplication] openURL:url options:nil];
	}


|
| *With Swift*
-------------

.. code-block:: swift

	func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
		let shouldOpen = GTVManager.shared().gtvApplication(app, open: url, options: options)
		return shouldOpen;
	}

|
| *If you use SceneDelegate in your project, let set this handle at call back openURLContexts.*

.. code-block:: swift

    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let url = URLContexts.first?.url else {
            return
        }
        
        GTVManager.shared().gtvApplication(UIApplication.shared, open: url, options: [:])
    }