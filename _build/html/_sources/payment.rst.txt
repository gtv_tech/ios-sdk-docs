.. GTVSDKDoc documentation master file, created by
   sphinx-quickstart on Thu Mar 14 15:46:07 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Payment
=====================================

Import GTVLib to your class

.. code-block:: objective-c

	//#import <GTVLib/GTVLib.h> // through cocoaPods
	#import <GTVFramework/GTVManager.h> // without cocoaPods


1. Payment from Webview
########

Payment from Webview with orderID, serverID, serverName, roleID, roleName, userID, channel, levels

|
| *Objective-c syntax*
-------------

.. code-block:: objective-c

    [[GTVManager sharedManager] showPaymentWithProductId:@"" serverId:@"serverd" roleId:@"roleId" levels:@"levels" extinfo:@"extention infomation" callback:^(GTVIAPStatus status, NSString * _Nonnull message) {
        //
    }];

* Example: *


.. code-block:: objective-c

    // show all product : set productId = @""
    if ([[GTVManager sharedManager] getPaymentToken]) {
        
        [[GTVManager sharedManager] showPaymentWithProductId:@"" serverId:@"1" roleId:@"49" levels:@"99" extinfo:@"extention infomation" callback:^(GTVIAPStatus status, NSString * _Nonnull message) {
            //
            switch (status) {
                case GTVRequestError:
                    //
                    break;
                    
                default:
                    break;
            }
            NSLog(@"Payment out: %u, mess: %@", status, message);
        }];
    }

    // show 1 product : set productId = @"product ??"
    if ([[GTVManager sharedManager] getPaymentToken]) {
        
        [[GTVManager sharedManager] showPaymentWithProductId:@"VMTQ007" serverId:@"1" roleId:@"49" levels:@"99" extinfo:@"extention infomation" callback:^(GTVIAPStatus status, NSString * _Nonnull message) {
            //
            switch (status) {
                case GTVRequestError:
                    //
                    break;
                    
                default:
                    break;
            }
            NSLog(@"Payment out: %u, mess: %@", status, message);
        }];
    }

|
| *Swift syntax*
-------------
 
.. code-block:: swift

    GTVManager.shared().showPayment(withOrderID: "orderID", serverID: "serverID", serverName: "serverName", roleID: "roleID", roleName: "roleName", userID: "userID", channel: "channel", levels: "levels") { (status, message) in
        //
    }


**Note: If you use us tokens(UserToken and PaymentToken) but this token is exprired, you need to call refresh it**

|
| *Objective-C syntax*
-------------

.. code-block:: objective-c

    [[GTVManager sharedManager] refreshTokenPaymentNow:^(BOOL isSuccess, NSString * _Nullable message) {
		//
	}];

	[[GTVManager sharedManager] refreshTokenAccountNow:^(BOOL isSuccess, NSString * _Nullable message) {
		//
	}];

|
| *Swift syntax*
-------------

.. code-block:: Swift

	GTVManager.shared().refreshTokenPaymentNow { (isSuccess, message) in
		//
	}
	GTVManager.shared().refreshTokenAccountNow { (isSuccess, message) in
		//
	}


2. Buy one product with GPoint
########

Buy product with Gpoint. params: productId, serverID, roleID, levels.

Params extinfo set empty (@""), if you need to use this param, notice us.

.. code-block:: objective-c

    [[GTVManager sharedManager] paymentLocalWithGPoint:@"VMTQ007" serverId:@"62" roleId:@"105324593808353" levels:@"30" extinfo:@"" callback:^(GTVIAPStatus status, NSString * _Nonnull message) {
        //
        switch (status) {
        case GTVRequestError:
            //
        break;
                    
        default:
            break;
        }
        NSLog(@"Payment out paymentLocalWithGPoint: %u, mess: %@", status, message);
    }];


**Note: Enum define**

.. code-block:: objective-c

    typedef enum {
        GTVPurchasing = 0,
        GTVPurchased, // Purchased with Apple and verify server error
        GTVRestored,
        GTVFailed,
        GTVRequestError, // Request Product ID error
        GTVVerified // Purchased with Apple and verify server success
    }GTVIAPStatus;

